CC := clang
CFLAGS := -std=c99 -Weverything
RM := rm -f

.PHONY : build clean check

build : gaplist.o

clean :
	$(RM) gaplist.o test

check : test
	./$<

gaplist.o : %.o : %.c %.h Makefile
	$(CC) $(CFLAGS) -c -o $@ $<

test : % : %.c gaplist.o
	$(CC) $(CFLAGS) -o $@ $^
