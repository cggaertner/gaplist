/*	Copyright 2012 Christoph Gärtner
	Distributed under the Boost Software License, Version 1.0
*/

#include "gaplist.h"

#include <assert.h>
#include <stdlib.h>
#include <string.h>

static inline size_t load(const struct gaplist *list)
{
	return list->head_size + list->tail_size;
}

static inline _Bool is_full(const struct gaplist *list)
{
	return load(list) == list->capacity;
}

static inline _Bool is_empty(const struct gaplist *list)
{
	return list->head_size == 0;
}

static inline _Bool has_tail(const struct gaplist *list)
{
	return list->tail_size > 0;
}

static inline char *get(const struct gaplist *list, size_t pos)
{
	return list->values + (pos % list->capacity) * list->value_size;
}

static inline char *head(const struct gaplist *list)
{
	return list->values + list->head_start * list->value_size;
}

static inline char *tail(const struct gaplist *list)
{
	return list->values + list->tail_start * list->value_size;
}

static inline char *last(const struct gaplist *list)
{
	return get(list, has_tail(list) ?
		list->tail_start + list->tail_size - 1 :
		list->head_start + list->head_size - 1);
}

static void copy_range(const struct gaplist *list, char *restrict dest,
	size_t offset, size_t pos, size_t count)
{
	size_t size = list->value_size;
	size_t overflow = 0;
	if(list->capacity - pos < count)
	{
		overflow = count - (list->capacity - pos);
		count -= overflow;
	}

	dest += offset * size;
	memcpy(dest, list->values + pos * size, count * size);
	if(!overflow) return;

	dest += count * size;
	memcpy(dest, list->values, overflow * size);
}

void gaplist_set_value_size(struct gaplist *list, size_t size)
{
	list->value_size = size;
}

size_t gaplist_size(const struct gaplist *list)
{
	return load(list);
}

void gaplist_clear(struct gaplist *list)
{
	free(list->values);
	list->values = NULL;
	list->capacity = 0;
	list->head_start = 0;
	list->head_size = 0;
	list->tail_size = 0;
}

_Bool gaplist_realloc(struct gaplist *list, size_t capacity, size_t offset)
{
	size_t count = load(list);
	if(capacity < count + offset)
		return 0;

	char *values = malloc(capacity * list->value_size);
	if(!values) return 0;

	if(count == 0)
		goto INIT;

	copy_range(list, values, offset,
		list->head_start, list->head_size);

	if(!has_tail(list))
		goto INIT;

	copy_range(list, values, offset + list->head_size,
		list->tail_start, list->tail_size);

INIT:
	free(list->values);
	list->values = values;
	list->capacity = capacity;
	list->head_start = offset;
	list->head_size = count;
	list->tail_size = 0;

	return 1;
}

void *gaplist_seek(const struct gaplist *list, size_t index)
{
	if(index < list->head_size)
		return get(list, list->head_start + index);

	index -= list->head_size;
	if(index < list->tail_size)
		return get(list, list->tail_start + index);

	return NULL;
}

void *gaplist_first(const struct gaplist *list)
{
	return is_empty(list) ? NULL : head(list);
}

void *gaplist_last(const struct gaplist *list)
{
	return is_empty(list) ? NULL : last(list);
}

void *gaplist_prev(const struct gaplist *list, const void *current_)
{
	const char *current = current_;
	if(!current) return NULL;

	assert(!is_empty(list));

	if(current == head(list))
		return NULL;

	if(has_tail(list) && current == tail(list))
		return get(list, list->head_start + list->head_size - 1);

	if(current == list->values)
		return get(list, list->capacity - 1);

	return (void *)(current - list->value_size);
}

void *gaplist_next(const struct gaplist *list, const void *current_)
{
	const char *current = current_;
	if(!current) return NULL;

	assert(!is_empty(list));

	if(current == get(list, load(list) - 1))
		return NULL;

	if(has_tail(list) && current == get(list, list->head_size - 1))
		return tail(list);

	if(current == list->values + (list->capacity - 1) * list->value_size)
		return list->values;

	return (void *)(current + list->value_size);
}

void *gaplist_shift(struct gaplist *list, void *restrict value)
{
	if(is_empty(list))
		return NULL;

	if(value) memcpy(value, head(list), list->value_size);
	--list->head_size;

	if(list->head_size > 0)
		list->head_start = (list->head_start + 1) % list->capacity;
	else
	{
		list->head_start = list->tail_start;
		list->head_size = list->tail_size;
		list->tail_size = 0;
	}

	return value;
}

void *gaplist_pop(struct gaplist *list, void *restrict value)
{
	if(is_empty(list))
		return NULL;

	if(value) memcpy(value, last(list), list->value_size);
	if(has_tail(list)) --list->tail_size;
	else --list->head_size;

	return value;
}

_Bool gaplist_push(struct gaplist *list,
	const void *restrict value, _Bool grow)
{
	if(is_full(list) &&
		(!grow || !gaplist_realloc(list, load(list) * 2 + 1, 0))) return 0;

	if(!has_tail(list))
	{
		char *end = is_empty(list) ? list->values :
			get(list, list->head_start + list->head_size);
		memcpy(end, value, list->value_size);
		++list->head_size;
		return 1;
	}

	char *start = head(list);
	char *end = get(list, list->tail_start + list->tail_size);
	if(start != end)
	{
		memcpy(end, value, list->value_size);
		++list->tail_size;
	}
	else
	{
		char *gap_start = get(list, list->head_start + list->head_size);
		char *gap_end = tail(list);

		size_t left = gap_end < end ? (size_t)(end - gap_end) : 0;
		size_t right = gap_start > start ? (size_t)(gap_start - start) : 0;

		if(left <= right)
		{
			if(gap_start < end)
			{
				memmove(gap_start, gap_end, left * list->value_size);
				memmove(gap_start + left * list->value_size, value,
					list->value_size);
				list->head_size += list->tail_size + 1;
				list->tail_size = 0;
			}
			else
			{
				memmove(list->values, gap_end, left * list->value_size);
				memmove(list->values + left * list->value_size, value,
					list->value_size);
				list->tail_start = 0;
				++list->tail_size;
			}
		}
		else
		{
			if(gap_end == list->values)
				gap_end = start + list->head_size * list->value_size;

			if(gap_end > start)
			{
				char *dest = gap_end - right;
				memmove(dest, start, right * list->value_size);
				memmove(end, value, list->value_size);
				list->head_size += list->tail_size + 1;
				list->tail_size = 0;
			}
			else
			{
				char *dest = list->values +
					(list->capacity - right) * list->value_size;
				memmove(dest, start, right * list->value_size);
				memmove(end, value, list->value_size);
				list->head_start = list->capacity - right;
				++list->tail_size;
			}
		}
	}

	return 1;
}
