/*	Copyright 2012 Christoph Gärtner
	Distributed under the Boost Software License, Version 1.0
*/

#ifndef GAPLIST_H_
#define GAPLIST_H_

#include <stddef.h>

struct gaplist
{
	char *values;
	size_t value_size;
	size_t capacity;
	size_t head_start;
	size_t head_size;
	size_t tail_start;
	size_t tail_size;
};

static const struct gaplist GAPLIST_NIL;

void gaplist_set_value_size(struct gaplist *list, size_t size);
size_t gaplist_size(const struct gaplist *list);
void gaplist_clear(struct gaplist *list);

_Bool gaplist_realloc(struct gaplist *list, size_t capacity, size_t offset);

_Bool gaplist_push(struct gaplist *list,
	const void *restrict value, _Bool grow);
void *gaplist_pop(struct gaplist *list, void *restrict value);

_Bool gaplist_unshift(struct gaplist *list,
	const void *restrict value, _Bool grow);
void *gaplist_shift(struct gaplist *list, void *restrict value);

void *gaplist_seek(const struct gaplist *list, size_t index);
void *gaplist_first(const struct gaplist *list);
void *gaplist_last(const struct gaplist *list);
void *gaplist_next(const struct gaplist *list, const void *current);
void *gaplist_prev(const struct gaplist *list, const void *current);

#define gaplist_push_value(LIST, TYPE, ...) \
	gaplist_push(LIST, &(TYPE){ __VA_ARGS__ }, 1)

#define gaplist_pop_value(LIST, TYPE) \
	(*(TYPE *)gaplist_pop(LIST, &(TYPE){ 0 }))

#define gaplist_unshift_value(LIST, TYPE, ...) \
	gaplist_unshift(LIST, &(TYPE){ __VA_ARGS__ }, 1)

#define gaplist_shift_value(LIST, TYPE) \
	(*(TYPE *)gaplist_shift(LIST, &(TYPE){ 0 }))

#endif
