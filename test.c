#include "gaplist.h"

#undef NDEBUG
#include <assert.h>

int main(void)
{
	{
		struct gaplist list[1] = { GAPLIST_NIL };
		gaplist_set_value_size(list, sizeof (int));
		assert(gaplist_size(list) == 0);

		assert(gaplist_push_value(list, int, 42));
		assert(gaplist_size(list) == 1);
		assert(list->capacity == 1);

		assert(gaplist_push_value(list, int, 0xbeef));
		assert(gaplist_size(list) == 2);
		assert(list->capacity == 3);

		assert(gaplist_pop_value(list, int) == 0xbeef);
		assert(gaplist_size(list) == 1);

		assert(gaplist_shift_value(list, int) == 42);
		assert(gaplist_size(list) == 0);

		gaplist_clear(list);
	}

	return 0;
}
